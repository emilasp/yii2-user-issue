<?php

namespace emilasp\userissue\widgets\UserIssueWidget;

use yii\web\AssetBundle;

/**
 * Class HtmlToCanvasLocalAsset
 * @package emilasp\userissue\widgets\UserIssueWidget
 */
class HtmlToCanvasLocalAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $js = [
        'html2canvas.min.js'
    ];
}