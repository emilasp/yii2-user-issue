<?php
use emilasp\userissue\models\UserIssue;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
?>

<?= Html::tag('a', '', [
    'href'        => '#send-issue',
    'class'       => 'issue-message',
    'data-toggle' => 'modal',
    'title'       => 'Сообщение администратору',
]) ?>

<div class="modal fade" id="send-issue" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="row">
        <div class="modal-content">
            <div class="modal-header bg-orange">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Сообщение администратору</h4>
            </div>


            <?php Pjax::begin(['id' => 'user-issue-form']); ?>

            <?php $form = ActiveForm::begin(['id' => 'user-issue-form-id', 'options' => ['data-pjax' => true]]); ?>


            <div class="modal-body" data-id="<?= $model->id ?>">

                <div class="row">
                    <div class="col-md-9">
                        <?= $form->field($model, 'title')
                            ->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('title')])
                            ->label(false) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'type')
                            ->dropDownList(UserIssue::$types)
                            ->label(false) ?>
                    </div>
                </div>

                <?= $form->field($model, 'text')
                    ->textarea(['rows' => 6, 'placeholder' => $model->getAttributeLabel('text')])
                    ->label(false) ?>

            </div>
            <div class="modal-footer">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

            <?php if (!$model->isNewRecord) : ?>

            <?php endif; ?>


            <?php Pjax::end(); ?>

            <canvas id="user-issue-canvas" width="500" height="200"></canvas>

        </div>
    </div>
</div>
<?php
$urlToSave = \yii\helpers\Url::toRoute(['/issue/user-issue/save-screen', 'id' => $model->id]);

$js = <<<JS
$('body').on("pjax:end", "#user-issue-form", function() {
    $('#send-issue').modal('hide');
    setTimeout(function() {
        var id = $('#user-issue-form .modal-body').data('id');

        if (id) {
            var canvas = document.querySelector("canvas");
            canvas.width  = $(window).width(); 
            canvas.height = $(window).height();
        
            html2canvas(document.body, {
                onrendered: function(canvas) {
                    $.ajax({
                        type: 'POST',
                        url: '{$urlToSave}',
                        dataType: "json",
                        data: 'id=' + id + '&data=' + canvas.toDataURL(),
                        success: function(msg) {
                            document.location.href = document.location.href;
                        },
                        error: function(){}
                    });
                }
            });
            
    
        } 
    }, 1000);
});            

JS;

$this->registerJs($js);
?>