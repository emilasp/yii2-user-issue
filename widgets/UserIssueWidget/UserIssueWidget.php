<?php
namespace emilasp\userissue\widgets\UserIssueWidget;

use emilasp\core\components\base\Widget;
use emilasp\userissue\models\IssueForm;
use emilasp\userissue\models\UserIssue;
use Yii;

/**
 * Class UserIssueWidget
 * @package emilasp\user\core\widgets\UserIssueWidget
 */
class UserIssueWidget extends Widget
{
    public $componentCart;

    private $model;

    public function init()
    {
        if (!Yii::$app->user->isGuest) {
            $this->registerAssets();
            $this->setForm();
            $this->saveForm();
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('message', [
                'model' => $this->model,
            ]);
        }
    }

    /**
     * Устанавливаем значения для формы
     */
    private function setForm()
    {
        /** @var IssueForm model */
        $this->model = new UserIssue();

        $this->model->status     = UserIssue::STATUS_DRAFTED;
        $this->model->env        = json_encode($_SERVER);
        $this->model->created_by = Yii::$app->user->id;
        $this->model->image      = '';
    }

    /**
     * Если данные пришли сохраняем
     */
    private function saveForm()
    {
        if ($this->model->load(Yii::$app->request->post()) && $this->model->save()) {
            Yii::$app->session->setFlash('success', 'Сообщение успешно отправлено');
        }
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        UserIssueWidgetAsset::register($this->view);
    }
}
