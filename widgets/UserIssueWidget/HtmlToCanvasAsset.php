<?php

namespace emilasp\userissue\widgets\UserIssueWidget;

use yii\web\AssetBundle;

/**
 * Class HtmlToCanvasAsset
 * @package emilasp\userissue\widgets\UserIssueWidget
 */
class HtmlToCanvasAsset extends AssetBundle
{
    public $sourcePath = '@bower/html2canvas';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $js = [
        'build/html2canvas.min.js'
    ];
}