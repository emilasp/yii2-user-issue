<?php

namespace emilasp\userissue\widgets\UserIssueWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ImCart
 * @package emilasp\userissue\widgets\UserIssueWidget
 */
class UserIssueWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'emilasp\userissue\widgets\UserIssueWidget\HtmlToCanvasLocalAsset',
    ];

    public $css = [
        'issue'
    ];
   /* public $js = [
        'issue'
    ];*/
}
