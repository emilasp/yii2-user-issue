<?php

namespace emilasp\userissue\controllers;

use emilasp\core\components\base\Controller;
use emilasp\core\helpers\FileHelper;
use Yii;
use emilasp\userissue\models\UserIssue;
use emilasp\userissue\models\search\UserIssueSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserIssueController implements the CRUD actions for UserIssue model.
 */
class UserIssueController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete', 'save-screen'],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'save-screen',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'      => ['POST'],
                    'save-screen' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserIssue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new UserIssueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserIssue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, UserIssue::className()),
        ]);
    }

    /**
     * Creates a new UserIssue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserIssue();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserIssue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, UserIssue::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserIssue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id, UserIssue::className());

        if ($model->status !== UserIssue::STATUS_DELETED) {
            $model->status = UserIssue::STATUS_DELETED;
            $model->save();
        } else {
            $model->delete();
        }

        return $this->redirect(['index']);
    }


    /**
     * Сохраняем скриншот браузера
     *
     */
    public function actionSaveScreen()
    {
        $id   = Yii::$app->request->post('id');
        $data = Yii::$app->request->post('data');

        $data = str_replace('data:image/png;base64,', '', $data);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);

        $path = Yii::getAlias(Yii::$app->getModule('issue')->screenPath);

        if (!is_dir($path)) {
            FileHelper::createDirectory($path);
        }

        file_put_contents($path . $id . '.png', $data);

        $model = $this->findModel($id, UserIssue::className());

        $model->image  = $id . '.png';
        $model->status = UserIssue::STATUS_NEW;
        $model->save();

        return $this->setAjaxResponse(self::AJAX_STATUS_SUCCESS);
    }
}
