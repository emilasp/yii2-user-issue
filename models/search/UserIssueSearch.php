<?php

namespace emilasp\userissue\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\userissue\models\UserIssue;

/**
 * UserIssueSearch represents the model behind the search form about `backend\modules\users\models\UserIssue`.
 */
class UserIssueSearch extends UserIssue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['title', 'text', 'image', 'env', 'answer', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserIssue::find();

        if (!Yii::$app->user->can(Yii::$app->getModule('issue')->roleAdmin)) {
            $query->andWhere([
                'created_by' => Yii::$app->user->id,
                'status'     => [self::STATUS_NEW, self::STATUS_DRAFTED, self::STATUS_ENABLED]
            ]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => ['status' => SORT_ASC, 'created_at' => SORT_DESC],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'type'       => $this->type,
            'status'     => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'env', $this->env])
            ->andFilterWhere(['like', 'answer', $this->answer]);

        return $dataProvider;
    }
}
