<?php

namespace emilasp\userissue\models;

use common\models\User;
use emilasp\core\components\base\ActiveRecord;
use emilasp\user\core\rbac\PhpManager;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "users_issue".
 *
 * @property integer $id
 * @property integer $type
 * @property string  $title
 * @property string  $text
 * @property string  $image
 * @property string  $env
 * @property string  $answer
 * @property integer $status
 * @property string  $created_at
 * @property string  $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User    $createdBy
 * @property User    $updatedBy
 */
class UserIssue extends ActiveRecord
{
    const STATUS_DRAFTED  = 0;
    const STATUS_NEW      = 1;
    const STATUS_ENABLED  = 2;
    const STATUS_DISABLED = 3;
    const STATUS_DELETED  = 4;

    public static $statuses = [
        self::STATUS_DRAFTED  => 'Черновик',
        self::STATUS_NEW      => 'Новый',
        self::STATUS_ENABLED  => 'Ответ получен',
        self::STATUS_DISABLED => 'Закрыт',
        self::STATUS_DELETED  => 'Удалён',
    ];

    const TYPE_ERROR   = 1;
    const TYPE_DESIGN  = 2;
    const TYPE_UPGRADE = 3;
    const TYPE_OTHER   = 4;

    public static $types = [
        self::TYPE_ERROR   => 'Ошибка',
        self::TYPE_DESIGN  => 'Оформление',
        self::TYPE_UPGRADE => 'Улучшение',
        self::TYPE_OTHER   => 'Другое',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_issue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['text'], 'required'],
            [['text', 'env'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'image'], 'string', 'max' => 255],
            [['answer'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'type'       => 'Тип',
            'title'      => 'Заголовок',
            'text'       => 'Сообщение',
            'image'      => 'Скриншот',
            'env'        => 'Окружение',
            'answer'     => 'Ответ',
            'status'     => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Updated At',
            'created_by' => 'Автор',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Получаем пользователя
     *
     * @return mixed
     */
    public function getUser()
    {
        if ($this->created_by && is_callable(Yii::$app->getModule('issue')->userCall)) {
            $function = Yii::$app->getModule('issue')->userCall;
            return $function($this->created_by);
        }
        return null;
    }

    /**
     * Получаем пользователя
     *
     * @return mixed
     */
    public function getUsername()
    {
        if ($this->user) {
            $userField = Yii::$app->getModule('issue')->userNameField;
            return $this->user->{$userField};
        }
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->status === UserIssue::STATUS_NEW) {
            if ($emails = Yii::$app->getModule('issue')->targetEmails) {
                $date   = Yii::$app->formatter->asDate($this->created_at);
                $type   = UserIssue::$types[$this->type];
                $status = UserIssue::$statuses[$this->status];
                $env    = VarDumper::dumpAsString(json_decode($this->env), 10, true);

                $html = <<<HTML
<h3>{$this->title} <small>({$this->username}: {$this->created_by})</small></h3>
<table>
    <tbody>
        <tr><td><strong>Дата:</strong></td><td>{$date}</td></tr>
        <tr><td><strong>Пользователь:</strong></td><td>{$this->username}({$this->created_by})</td></tr>
        <tr><td><strong>Тип:</strong></td><td>{$type}</td></tr>
        <tr><td><strong>Статус:</strong></td><td>{$status}</td></tr>
        <tr><td><strong>Заголовок:</strong></td><td>{$this->title}</td></tr>
        <tr><td colspan="2"><strong>Сообщение:</strong></td></tr>
        <tr><td colspan="2">{$this->text}</td></tr>
        <tr><td><strong>Ответ:</strong></td><td>{$this->answer}</td></tr>
    </tbody>
</table>
<hr />
<div><strong>ENV:</strong></div>
<div><strong>{$env}</strong></div>


HTML;
                $mail = Yii::$app->mailer->compose()
                    ->setTo($emails)
                    ->setSubject('Okremont User ISSUE')
                    ->setTextBody($this->text)
                    ->setHtmlBody($html);

                $file = Yii::getAlias(Yii::$app->getModule('issue')->screenPath) . $this->image;

                if (is_file($file)) {
                    $mail->attach($file, ['fileName' => 'screenshot.png', 'contentType' => 'image/png']);
                }
                $mail->send();
            }
        }
    }
}
