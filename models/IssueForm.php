<?php
namespace emilasp\userissue\models;

/**
 * Class IssueForm
 * @package emilasp\userissue\models
 */
class IssueForm extends UserIssue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status', 'created_by'], 'integer'],
            [['type', 'title', 'text'], 'required'],
            [['text', 'env', 'image'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }
}
