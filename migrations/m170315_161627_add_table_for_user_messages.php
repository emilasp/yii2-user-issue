<?php
use yii\db\Migration;

class m170315_161627_add_table_for_user_messages extends Migration
{
    public function up()
    {
        $this->createTable('users_issue', [
            'id'         => $this->primaryKey(11),
            'type'       => $this->integer(1),
            'title'      => $this->string(255),
            'text'       => $this->text()->notNull(),
            'image'      => $this->string(255),
            'env'        => $this->text(),
            'answer'     => $this->string(250),
            'status'     => $this->smallInteger(1)->defaultValue(0)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);

        /*$this->addForeignKey(
            'fk_user_issue_created_by',
            'users_issue',
            'created_by',
            'user',
            'id'
        );
        $this->addForeignKey(
            'fk_user_issue_updated_by',
            'users_issue',
            'updated_by',
            'user',
            'id'
        );*/

        $this->createIndex('users_issue_status_type_idx', 'users_issue', ['status', 'type']);
        $this->createIndex('users_issue_created_idx', 'users_issue', ['created_at']);
    }

    public function down()
    {
        $this->dropTable('users_issue');
    }
}
