<?php

use emilasp\userissue\models\search\UserIssueSearch;
use emilasp\userissue\models\UserIssue;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel UserIssueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Запросы пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-issue-index box box-primary">

    <div class="box-body">
        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'image',
                    'value'     => function ($model, $key, $index, $column) {
                        $path = Yii::getAlias(Yii::$app->getModule('issue')->screenPath) . '/' . $model->image;
                        $src  = Yii::getAlias(Yii::$app->getModule('issue')->screenUrl) . '/' . $model->image;
                        if (is_file($path)) {
                            return Html::a(
                                Html::img($src, ['width' => '50px']),
                                $src,
                                ['data-pjax' => 0, 'target' => '_blank']
                            );
                        }
                        return null;
                    },
                    'class'     => '\yii\grid\DataColumn',
                    'format'    => 'raw',
                ],
                [
                    'attribute' => 'type',
                    'value'     => function ($model, $key, $index, $column) {
                        return UserIssue::$types[$model->type];
                    },
                    'class'     => '\yii\grid\DataColumn',
                    'filter'    => UserIssue::$types,
                ],
                'title',
                'text:ntext',
                [
                    'attribute' => 'status',
                    'value'     => function ($model, $key, $index, $column) {
                        return UserIssue::$statuses[$model->status];
                    },
                    'class'     => '\yii\grid\DataColumn',
                    'filter'    => UserIssue::$statuses,
                ],
                [
                    'attribute' => 'created_at',
                    'value'     => function ($model, $key, $index, $column) {
                        return Yii::$app->formatter->asDate($model->created_at);
                    },
                    'class'     => '\yii\grid\DataColumn',
                    'format'    => 'raw',
                ],

                [
                    'attribute' => 'created_by',
                    'value'     => function ($model, $key, $index, $column) {
                        return $model->username;
                    },
                    'class'     => '\yii\grid\DataColumn',
                    'format'    => 'raw',
                ],
                // 'updated_at',
                // 'created_by',
                // 'updated_by',

                ['class' => 'yii\grid\ActionColumn'],
            ],
            'rowOptions'   => function ($model, $key, $index, $grid) {
                $rowOptions = [];
                switch ($model->type) {
                    case UserIssue::TYPE_ERROR:
                        $rowOptions = ['class' => 'danger'];
                        break;
                    case UserIssue::TYPE_DESIGN:
                        $rowOptions = ['class' => 'primary'];
                        break;
                    case UserIssue::TYPE_OTHER:
                        $rowOptions = ['class' => 'info'];
                        break;
                    case UserIssue::TYPE_UPGRADE:
                        $rowOptions = ['class' => 'success'];
                        break;
                }
                return $rowOptions;
            },
        ]); ?>

        <?php Pjax::end(); ?>

    </div>
</div>
