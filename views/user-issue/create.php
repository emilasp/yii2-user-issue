<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\UserIssue */

$this->title = 'Create User Issue';
$this->params['breadcrumbs'][] = ['label' => 'User Issues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-issue-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
