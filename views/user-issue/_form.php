<?php

use emilasp\userissue\models\UserIssue;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\UserIssue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-issue-form">

    <div class="row bg-primary">
        <div class="col-md-6">
            <?= Yii::$app->formatter->asDate($model->created_at) ?>
        </div>
        <div class="col-md-6 text-right">
            <?= $model->username ?>
        </div>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList(UserIssue::$types) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'answer')->textarea(['rows' => 4]) ?>

            <?= $form->field($model, 'status')->dropDownList(UserIssue::$statuses) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

            <?= VarDumper::dumpAsString(json_decode($model->env), 10, true) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

