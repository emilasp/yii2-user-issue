<?php
namespace emilasp\userissue;

use emilasp\core\CoreModule;


/**
 * Class UsersIssueModule
 * @package emilasp\userissue
 */
class UsersIssueModule extends CoreModule
{
    public $userCall;
    public $userNameField;
    public $screenPath   = '@webroot/uploads/issue/';
    public $screenUrl    = '@web/uploads/issue/';
    public $roleAdmin    = 'admin';
    public $targetEmails = [];
}
